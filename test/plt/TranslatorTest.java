package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void ShouldReturnInputPhrase() throws PLTException{
		String inputPhrase = "hello world";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void TranslationInputPhrase() throws PLTException{
		String inputPhrase = "";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals(Translator.NIL, translator.translate());
	}

	@Test
	public void TranslationInputPhraseStartingWithAEndingY() throws PLTException{
		String inputPhrase = "any";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseStartingWithUEndingY() throws PLTException{
		String inputPhrase = "utility";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseStartingWithVowelEndingVowel() throws PLTException{
		String inputPhrase = "apple";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseStartingWithVowelEndingConsonant() throws PLTException{
		String inputPhrase = "ask";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseMoveSingleConsonantFromStartToEnd() throws PLTException{
		String inputPhrase = "hello";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseMoveMoreConsonantsFromStartToEnd() throws PLTException{
		String inputPhrase = "known";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseWithMoreWordsWithoutDash() throws PLTException{
		String inputPhrase = "hello world";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseWithMoreWordsWithDash() throws PLTException{
		String inputPhrase = "well-being";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellway-eingbay", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseWithMoreWordsWithDashAndSpace() throws PLTException{
		String inputPhrase = "well well-being";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellway ellway-eingbay", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseWithMoreWordsContainsPunctuations() throws PLTException{
		String inputPhrase = "hello world!";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway!", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseSingleWordWithPunctuation() throws PLTException{
		String inputPhrase = "hello!";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay!", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseOneWordPunctuation() throws PLTException{
		String inputPhrase = "known?";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ownknay?", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseThreeWordsWithSpacesAndPunctuation() throws PLTException{
		String inputPhrase = "creature carriage adheasive discovery flavor!";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("eaturecray arriagecay adheasiveyay iscoveryday avorflay!", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseWordsWithDashAndPunctuation() throws PLTException{
		String inputPhrase = "banana!-shake!";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ananabay!-akeshay!", translator.translate());
	}
	
	@Test
	public void TranslationInputPhraseShouldThrowPigLatinExceptionDoubleSpaces() throws PLTException{
		
		try {
            
            String inputPhrase = "banana  shake";
            
            Translator translator = new Translator(inputPhrase);
            
            translator.translate();
        } 
        
        catch(PLTException error) {
        	
            fail();
            
        }
		
	}
	
	@Test
	public void TranslationInputPhraseShouldThrowPigLatinExceptionDoubleDashes() throws PLTException{
		
		try {
            
            String inputPhrase = "banana--shake";
            
            Translator translator = new Translator(inputPhrase);
            
            translator.translate();
        } 
        
        catch(PLTException error) {
        	
            fail();
            
        }
		
	}
	
	
	
	
}
