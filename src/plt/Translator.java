package plt;

public class Translator {

	public static final String NIL = "nil";
	
	
	private String phrase;
	
	
	public Translator(String inputPhrase) throws PLTException {

		phrase = inputPhrase;
	}

	public String getPhrase() throws PLTException{

		return phrase;
	}
	
	
	public String translate() throws PLTException{
			
		if(phrase.length() > 0) {
					
			if( phrase.split(" ").length > 1 ) {
						
				return translateSplit( phrase.split(" "), " " );	
				
			} else if( phrase.split("-").length > 1) {
				
				return translateSplit( phrase.split("-"), "-" );
				
			} else {	

				return translatePhraseWithSingleWord();
				
			} 
			
		}	

		return NIL;
	}
	
	public boolean startsWithVowel() throws PLTException{
		return phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") 
				|| phrase.startsWith("o") || phrase.startsWith("u");
	}
	
	public boolean endsWithVowel() throws PLTException{
		return phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") 
				|| phrase.endsWith("o") || phrase.endsWith("u");
	}
	
	//riavvia il plugin Butterfly
	
	public boolean isConsonant(char c) throws PLTException{
		return !( c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' );
	}
	
	
	public int countConsonants() throws PLTException{
		
		int consonantCounter = 0;
		
		for(int i = 0; i < phrase.length(); i++) {
			if( isConsonant( phrase.charAt(i) )) {
				consonantCounter++;
			} else {
				return consonantCounter;
			}
		}
		return consonantCounter;
		
	}

	public String translateSplit(String[] s, String separator) throws PLTException{
		
		StringBuilder translatedSplittedString = new StringBuilder();

		for(int i = 0; i < s.length; i++) {
			
			if( s[i].equals(" ")) {
				throw new PLTException("Splitted String contains more spaces/dashes between certain words!");
			}
			
			Translator t = new Translator(s[i]);
				
			translatedSplittedString.append(t.translate());	
				
			if( i < s.length - 1) {			
				translatedSplittedString.append(separator);				
			}
				
		}
		
		return translatedSplittedString.toString();
	}
	
	public boolean isPunctuation(char s) throws PLTException{
		return (s == '.' || s == ',' || s == ';' || s == ':' || s == '?' || s == '!' ||
				s == '\'' || s == '(' || s == ')');
	}
	
	public int checkForPunctuation(String p) throws PLTException{
		
		for(int i = 0; i < p.length(); i++) {
			
			if( isPunctuation( p.charAt(i) ) ) {
				return i;
			}
			
		}
		
		return -1;
		
	}
	
	public String translatePhraseWithSingleWord() throws PLTException{
		
		int punctuationIndex = -1;
		punctuationIndex = checkForPunctuation(phrase);
		
		if(startsWithVowel()) {

			return FirstLetterVowel(punctuationIndex);	

		} 
		
		if(!startsWithVowel()){
			
			return FirstLetterConsonant(punctuationIndex);
			
		}
		
		return NIL;
		
	}
	
	public String FirstLetterVowel(int punctuationCase) throws PLTException{
		
		if(punctuationCase == -1) {
			
			if( phrase.endsWith("y") ){
				return phrase + "nay";
			} 
			if ( endsWithVowel() ) {
				return phrase + "yay";
			} 
			if ( !endsWithVowel() ) {
				return phrase + "ay";
			}
			
		} else {
			
			if( phrase.endsWith("y") ){
				return phrase.substring(0,punctuationCase) + "nay" + phrase.charAt(punctuationCase);
			} 
			if ( endsWithVowel() ) {
				return phrase.substring(0,punctuationCase) + "yay" + phrase.charAt(punctuationCase);
			} 
			if ( !endsWithVowel() ) {
				return phrase.substring(0,punctuationCase) + "ay" + phrase.charAt(punctuationCase);
			}
				
		}
		
		return NIL;
		
	}
	
	public String FirstLetterConsonant(int punctuationCase) throws PLTException{
				
		int nCons = countConsonants();
		
		if( punctuationCase == -1 ) {
			if(nCons == 1) {
				return phrase.substring(1) + phrase.charAt(0) + "ay";
			} else {
				return phrase.substring(nCons) + phrase.substring(0,nCons) + "ay";
			}
		} else {
			if(nCons == 1) {
				return phrase.substring(1,punctuationCase) + phrase.charAt(0) + "ay" + phrase.charAt(punctuationCase);
			} else {
				return phrase.substring(nCons, punctuationCase) + phrase.substring(0,nCons) + "ay" + phrase.charAt(punctuationCase);
			}
		}
		
		
	}
	
	
	
	
}
