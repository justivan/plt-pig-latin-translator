package plt;

public class PLTException extends Exception {

	private static final long serialVersionUID = 1L;

	public PLTException(String message) {
		super(message);
	}
	
}
